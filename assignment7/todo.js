let todoItems = new Array();

/**
 * If the todo item is deleted, remove it from the DOM. If it's not deleted, create a new DOM element
 * and add it to the DOM
 * @param todo - The todo object that we want to render.
 * @returns the value of the variable node.
 */
const renderTasks = (todo) => {
    localStorage.setItem('todoItemsReference', JSON.stringify(todoItems));
    const list = document.querySelector('.js-todo-list');
    const item = document.querySelector(`[data-key='${todo.id}']`);

    if (todo.deleted) {
        item.remove();
        if (todoItems.length === 0) list.innerHTML = '';
        return;
    }

    const isChecked = todo.checked ? 'done' : '';
    const node = document.createElement('li');
    node.setAttribute('class', `todo-item ${isChecked}`);
    node.setAttribute('data-key', todo.id);
    node.innerHTML = `
    <input id="${todo.id}" type="checkbox"/>
    <label for="${todo.id}" class="tick js-tick"></label>
    <span>${todo.text}</span>
    <button class="delete-todo js-delete-todo">
    <svg><use href="#delete-icon"></use></svg>
    </button>`;

    item ? list.replaceChild(node, item) : list.prepend(node);
};

/**
 * It takes a string as an argument, creates a new object with that string as the text property, and
 * adds that object to the todoItems array
 * @param text - The text of the todo item.
 */
const addTask = (text) => {
    const todo = {
        text,
        checked: false,
        id: Date.now(),
    };

    todoItems.push(todo);
    renderTasks(todo);
};

/**
 * It finds the index of the todo item in the todoItems array, toggles the checked property, and then
 * re-renders the todo item
 * @param key - The key of the todo item that was clicked.
 */
const toggleDone = (key) => {
    const index = todoItems.findIndex((item) => item.id === Number(key));
    todoItems[index].checked = !todoItems[index].checked;
    renderTasks(todoItems[index]);
};

/**
 * It takes a key, finds the index of the todo item with that key, creates a new todo object with the
 * deleted property set to true, and then filters the todoItems array to remove the todo item with the
 * matching key
 * @param key - The key of the todo item to be deleted.
 */
const deleteTask = (key) => {
    const index = todoItems.findIndex((item) => item.id === Number(key));
    const todo = {
        deleted: true,
        ...todoItems[index],
    };

    todoItems = todoItems.filter((item) => item.id !== Number(key));
    renderTasks(todo);
};

const updateStatus = () => {
    document.querySelector('.todo-status').innerHTML = `${
        todoItems.filter((element) => element.checked === true).length
    }/${todoItems.length} completed`;
};

const form = document.querySelector('.js-form');
/* It's adding an event listener to the form element. When the form is submitted, it prevents the
default action,
gets the value of the input, and if the value is not empty, it calls the addTask function with the
value of the input. */
form.addEventListener('submit', (e) => {
    e.preventDefault();
    const input = document.querySelector('.js-todo-input');

    const text = input.value.trim();
    if (text !== '') {
        addTask(text);
        input.value = '';
        input.focus();
        updateStatus();
    }
});

const list = document.querySelector('.js-todo-list');
/* It's adding an event listener to the list element. When the list is clicked, it checks if the
target element has the class js-tick. If it does, it gets the data-key attribute of the parent
element
and calls the toggleDone function with that value. If the target element has the class
js-delete-todo,
it gets the data-key attribute of the parent element and calls the deleteTask function with that
value. */
list.addEventListener('click', (e) => {
    if (e.target.classList.contains('js-tick')) {
        const itemKey = e.target.parentElement.dataset.key;
        toggleDone(itemKey);
        updateStatus();
    }

    if (e.target.classList.contains('js-delete-todo')) {
        const itemKey = e.target.parentElement.dataset.key;
        deleteTask(itemKey);
        updateStatus();
    }
});

document.addEventListener('DOMContentLoaded', () => {
    const reference = localStorage.getItem('todoItemsReference');
    if (reference) {
        todoItems = JSON.parse(reference);
        todoItems.forEach((t) => {
            renderTasks(t);
        });
    }
});

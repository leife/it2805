
const income = document.getElementById('income');
const wealth = document.getElementById('wealth');
const tax = document.getElementById('tax');

const calulateTax = () => {
    const incomeValue = income.value || 0;
    const wealthValue = wealth.value || 0;
    tax.value = (0.35 * parseInt(incomeValue)) + (0.25 * parseInt(wealthValue));
}

income.addEventListener('input', calulateTax);
wealth.addEventListener('input', calulateTax);


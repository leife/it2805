const taxForms = [
    {
        realName: 'Bruce Wayne',
        income: 750000,
        wealth: 300000,
    },
    {
        realName: 'John Blake',
        income: 440000,
        wealth: 832000,
    },
    {
        realName: 'Selina Kyle',
        income: 640000,
        wealth: 432000,
    },
];

const canvas = document.getElementById('chart');
const context = canvas.getContext('2d');
const interval = 50;

/* Draw a line from (fromX, fromY) to (toX, toY) */
function drawLine(fromX, fromY, toX, toY) {
    context.beginPath();
    context.moveTo(toX, toY);
    context.lineTo(fromX, fromY);
    context.stroke();
}

/* Draw a text (string) on (x, y) */
function drawText(text, x, y) {
    context.fillStyle = 'black';
    context.fillText(text, x, y);
}

/* Draw a text and with a line to its right */
function drawLineWithText(text, fromX, fromY, toX, toY) {
    drawText(text, fromX - 50, fromY + 3);
    drawLine(fromX, fromY, toX, toY);
}

/* Insert your code here. */
const lineWidth = canvas.width - 85;
for (let i = 1; i <= 10; i++) {
    drawLineWithText(
        i * 100000,
        interval,
        canvas.height - interval * i,
        lineWidth,
        canvas.height - interval * i
    );
}

const drawLegend = (text, fillStyle, x, y) => {
    const width = 36;
    const height = 22;
    const fontHeight = parseInt(context.font.match(/\d+/), 10);
    drawText(text, x, y - height / 2 + fontHeight / 2);
    context.fillStyle = fillStyle;
    context.fillRect(x - (width + 6), y - height, width, height);
};

drawLegend('Income', '#ff0000', canvas.width - 35, 70);
drawLegend('Wealth', '#0000ff', canvas.width - 35, 100);

const drawBar = (x, y, width, height, fillStyle) => {
    context.fillStyle = fillStyle;
    context.fillRect(x, y, width, -height);
};

let column = 0;
const barWidth = lineWidth / 10;

taxForms.map((taxForm) => {
    const income =
        (taxForm.income / ((100000 * canvas.height) / interval)) *
        canvas.height;
    const wealth =
        (taxForm.wealth / ((100000 * canvas.height) / interval)) *
        canvas.height;

    column++;
    drawBar(barWidth * column, canvas.height, barWidth, income, '#ff0000');
    column++;
    drawBar(barWidth * column + 6, canvas.height, barWidth, wealth, '#0000ff');
    column++;
});

/* Part 2 */
console.log('PART 2');
/* Printing the numbers 1 to 20 to the console. */
for (let i = 1; i <= 20; i++) console.log(i);

/* Part 3 */
console.log('PART 3');

const numbers = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
];

for (const element of numbers) {
  element % 3 === 0 && element % 5 === 0
    ? console.log('eplekake')
    : element % 5 === 0
    ? console.log('kake')
    : element % 3 === 0
    ? console.log('eple')
    : console.log(element);
}

/* Part 4 */
const title = document.getElementById('title');
const titleText = 'Hello, JavaScript';
title.innerHTML = titleText;

/* Part 5 */
const changeDisplay = async () => {
  const magicBox = document.getElementById('magic');
  magicBox.style.display = 'none';
};

const changeVisibility = async () => {
  const magicBox = document.getElementById('magic');
  magicBox.style.display = 'block';
  magicBox.style.visibility = 'hidden';
};

const reset = async () => {
  const magicBox = document.getElementById('magic');
  magicBox.style.display = 'block';
  magicBox.style.visibility = 'visible';
};

const buttons = document.getElementsByTagName('button');
const displayButton = buttons[0];
const visibilityButton = buttons[1];
const resetButton = buttons[2];

displayButton.addEventListener('click', changeDisplay);
visibilityButton.addEventListener('click', changeVisibility);
resetButton.addEventListener('click', reset);

/* Part 6 */
const technologies = [
  'HTML5',
  'CSS3',
  'JavaScript',
  'Python',
  'Java',
  'AJAX',
  'JSON',
  'React',
  'Angular',
  'Bootstrap',
  'Node.js',
];

const techList = document.getElementById('tech');

for (const element of technologies) {
  const listItem = document.createElement('li');
  listItem.innerHTML = element;
  techList.appendChild(listItem);
}
